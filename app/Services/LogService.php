<?php


namespace App\Services;


use App\Models\Log;
use GuzzleHttp\Client;

class LogService
{
    const CMD = "
    cd /var/www/project/monitoring/
        if test `stat -c %b type3_errors.log` -gt 0; then echo 'Type3 log is not empty';fi
        if test `stat -c %Y type2.log` -lt `date --date='now -1 minutes' +%s`; then echo 'Process did not run for at least 1 minute';fi
        if test `grep -c 'crashed' type4_errors.log` -gt 0; then echo 'There is a broken table';fi
        if [ -f type1.lock ] && test `stat -c %Y type1.lock` -lt `date --date='now -5 minutes' +%s`; then echo 'Process has been stuck for at least 5 minutes';fi";
    public function execCommand(){
        exec(self::CMD,$output);
        $content = '';
        foreach ($output as $error){
            $content .= $error."\n";
        }

//        $lastContent = file_exists('/var/www/project/monitoring/monitor.last') ?
//            file_get_contents('/var/www/project/monitoring/monitor.last') : '';
        //read lastest from database
        $lastContent = Log::latest()->first();
        if(empty($lastContent) || $content != $lastContent['data_log']){
            $this->saveLog($content);
            $client = new Client();
            $client->request('POST', 'http://localhost:8000/api/log/sendmail', [
                'form_params'=>[
                    'data'=>$content,
                ]]);
        }
    }
    private function saveLog($content){
        return Log::create(['data_log'=>$content]);
    }
}
