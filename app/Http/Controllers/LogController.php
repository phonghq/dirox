<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LogController extends Controller
{
    public function sendMail(Request $request){
        $contents = $request->data ?? 'Back to normal';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'. "\r\n";
        $headers .= 'From: Benjamin CHERY <benjamin.chery@domain.com>'. "\r\n";
        $email = env('mail_to','benjamin.chery@domain.com,firstname.lastname@domain.com');
        $email = explode(',',$email);
        Mail::send([],[],function ($message) use ($contents, $email) {
            $message->to($email)
                ->subject('Monitoring '.date('Y-m-d'))
                // here comes what you want
                ->setBody($contents, 'text/html'); // assuming text/plain
        });
    }
}
